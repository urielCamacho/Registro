----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:43:30 10/27/2017 
-- Design Name: 
-- Module Name:    NOR_GATEWAY - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NOR_GATEWAY is
    Port ( in_a_nor : in  STD_LOGIC;
           in_b_nor : in  STD_LOGIC;
           out_nor : out  STD_LOGIC);
end NOR_GATEWAY;

architecture Behavioral of NOR_GATEWAY is
begin
	out_NOR<=in_a_nor NOR in_b_nor;

end Behavioral;

