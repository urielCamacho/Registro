----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:55:37 10/27/2017 
-- Design Name: 
-- Module Name:    NAND_GATE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NAND_GATE is
    Port ( in_a_NAND : in  STD_LOGIC;
           in_b_NAND : in  STD_LOGIC;
           out_NAND : out  STD_LOGIC);
end NAND_GATE;

architecture Behavioral of NAND_GATE is

begin
	out_NAND<=in_a_NAND NAND in_b_NAND;

end Behavioral;

