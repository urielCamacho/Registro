----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:46:20 10/27/2017 
-- Design Name: 
-- Module Name:    NOT_GATEWAY - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NOT_GATEWAY is
    Port ( in_not : in  STD_LOGIC;
           out_not : out  STD_LOGIC);
end NOT_GATEWAY;

architecture Behavioral of NOT_GATEWAY is
begin
	out_not<= not in_not;
end Behavioral;

