----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:24:54 10/27/2017 
-- Design Name: 
-- Module Name:    NAND_GATEWAY - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NAND_GATEWAY is
    Port ( a_nand : in  STD_LOGIC;
           b_nand : in  STD_LOGIC;
           output_nand : out  STD_LOGIC);
end NAND_GATEWAY;

architecture Behavioral of NAND_GATEWAY is
	output_nand <= a_nand nand b_nand;
begin


end Behavioral;

