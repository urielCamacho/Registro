----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:58:27 10/27/2017 
-- Design Name: 
-- Module Name:    FLIP_FLOP_D - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FLIP_FLOP_D is
    Port ( S : in  STD_LOGIC;
           R : in  STD_LOGIC;
			  CLK : in STD_ULOGIC;
			  SQ : inout STD_LOGIC;
			  RQ : inout STD_LOGIC;
           Q : inout  STD_LOGIC;
           Qx : inout  STD_LOGIC);
end FLIP_FLOP_D;

architecture Behavioral of FLIP_FLOP_D is
	signal  clock : std_ulogic := '1';
	constant num_cycles : integer := 320;
	
	component NAND_GATEWAY
		port(in_a_nand,in_b_nand: in STD_LOGIC;
		output_nand: out STD_LOGIC);
	end component;
	
begin
	nand_first : NAND_GATEWAY port map(S,CLK,SQ);
	
	nand_second : NAND_GATEWAY port map(CLK, R, RQ);
	
	nand_third : NAND_GATEWAY port map(SQ,Qx,Q);
	
	nand_fourd : NAND_GATEWAY port map(Q,RQ,Qx);
	
	process
		begin
		
		for i in 1 to num_cycles loop
		clock <= '1';
      wait for 500000 ns;
      clock <= '0';
      wait for 500000 ns;
      -- clock period = 10 ns
		end loop;
	end process;
	
	not_component: NOT_GATEWAY port map(D_FF,Qx);
	
	nand_first_component: NAND_GATEWAY port map(D_FF,CP_FF,Q);
	
	nand_second_component: NAND_GATEWAY port map(not_component,CP_FF,Qx);	

	nor_first_component: NOR_GATEWAY port map(nand_first_component,first_signal,Q);
	
	nor_second_component: NOR_GATEWAY port map(D_FF,second_signal,Qx);
	
end Behavioral;

